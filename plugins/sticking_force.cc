#include "sticking_force.hh"

using namespace gazebo;

StickingForcePlugin::StickingForcePlugin() : ModelPlugin()
{
}

StickingForcePlugin::~StickingForcePlugin()
{
}

// --------------------- Callbacks ---------------------

// listen to the normal of each wheel (if active)

void StickingForcePlugin::callback_normal_front_left_wheel(ConstVector3dPtr &_msg)
{
  front_left_wheel_normal = ignition::math::Vector3d(_msg->x(), _msg->y(), _msg->z());
  //ROS_INFO("[sticking_force]: Normal front_left: [%f, %f, %f]", _msg->x(), _msg->y(), _msg->z());

  stickyForceOnFrontLeftWheel = ignition::math::Vector3d((magnitude)*front_left_wheel_normal.X(),
   (magnitude)*front_left_wheel_normal.Y(), (magnitude)*front_left_wheel_normal.Z());
}

void StickingForcePlugin::callback_normal_front_right_wheel(ConstVector3dPtr &_msg)
{
  front_right_wheel_normal = ignition::math::Vector3d(_msg->x(), _msg->y(), _msg->z());
  //ROS_INFO("[sticking_force]: Normal front_right: [%f, %f, %f]", _msg->x(), _msg->y(), _msg->z());

  stickyForceOnFrontRightWheel = ignition::math::Vector3d((magnitude)*front_right_wheel_normal.X(),
   (magnitude)*front_right_wheel_normal.Y(), (magnitude)*front_right_wheel_normal.Z());
}

void StickingForcePlugin::callback_normal_rear_left_wheel(ConstVector3dPtr &_msg)
{
  rear_left_wheel_normal = ignition::math::Vector3d(_msg->x(), _msg->y(), _msg->z());
  //ROS_INFO("[sticking_force]: Normal rear_left: [%f, %f, %f]", _msg->x(), _msg->y(), _msg->z());

  stickyForceOnRearLeftWheel = ignition::math::Vector3d((magnitude)*rear_left_wheel_normal.X(),
   (magnitude)*rear_left_wheel_normal.Y(), (magnitude)*rear_left_wheel_normal.Z());
}

void StickingForcePlugin::callback_normal_rear_right_wheel(ConstVector3dPtr &_msg)
{
  rear_right_wheel_normal = ignition::math::Vector3d(_msg->x(), _msg->y(), _msg->z());
  //ROS_INFO("[sticking_force]: Normal rear_right: [%f, %f, %f]", _msg->x(), _msg->y(), _msg->z());

  stickyForceOnRearRightWheel = ignition::math::Vector3d((magnitude)*rear_right_wheel_normal.X(),
   (magnitude)*rear_right_wheel_normal.Y(), (magnitude)*rear_right_wheel_normal.Z());
}

// listen to the net normal force at the origin of the robot

void StickingForcePlugin::callback_net_force(ConstVector3dPtr &_msg)
{
  netNormalForce = ignition::math::Vector3d(_msg->x(), _msg->y(), _msg->z());

  stickyForce = ignition::math::Vector3d((magnitude)*netNormalForce.X(),
   (magnitude)*netNormalForce.Y(), (magnitude)*netNormalForce.Z());
   //ROS_INFO("[sticking_force]: Sticky Force: [%f, %f, %f]", stickyForce.X(), stickyForce.Y(), stickyForce.Z());
}

// listen to the common channel about magnetic field

void StickingForcePlugin::callback_magnetic(ConstIntPtr &_msg)
{
  netMagneticFieldEnabled = _msg->data();
  //ROS_INFO("[sticking_force]: The net magnetic field is: %d", netMagneticFieldEnabled);
}


// listen if magnetic field is enabled in each wheel (if active)

void StickingForcePlugin::callback_magnetic_front_left_wheel(ConstIntPtr &_msg)
{
  magneticField_FrontLeftWheel_Enabled = _msg->data();
  //ROS_INFO("[sticking_force]: The FrontLeftWheel magnetic field is: %d", magneticField_FrontLeftWheel_Enabled);
}

void StickingForcePlugin::callback_magnetic_front_right_wheel(ConstIntPtr &_msg)
{
  magneticField_FrontRightWheel_Enabled = _msg->data();
  //ROS_INFO("[sticking_force]: The FrontRightWheel magnetic field is: %d", magneticField_FrontRightWheel_Enabled);
}

void StickingForcePlugin::callback_magnetic_rear_left_wheel(ConstIntPtr &_msg)
{
  magneticField_RearLeftWheel_Enabled = _msg->data();
  //ROS_INFO("[sticking_force]: The RearLeftWheel magnetic field is: %d", magneticField_RearLeftWheel_Enabled);
}

void StickingForcePlugin::callback_magnetic_rear_right_wheel(ConstIntPtr &_msg)
{
  magneticField_RearRightWheel_Enabled = _msg->data();
  //ROS_INFO("[sticking_force]: The RearRightWheel magnetic field is: %d", magneticField_RearRightWheel_Enabled);
}

// --------------------- Load ---------------------

void StickingForcePlugin::Load(physics::ModelPtr _parent, sdf::ElementPtr _sdf)
{
  std::string local_prefix = "/gazebo/default/";

  // Get the control elements
  magnitude = -atoi((_sdf->GetElement("magnitude")->Get<std::string>()).c_str());
  advancedMode = atoi((_sdf->GetElement("advancedMode")->Get<std::string>()).c_str());
  distributedForce = atoi((_sdf->GetElement("distributedForce")->Get<std::string>()).c_str());

  // Get the link names from the URDF
  robotBaseFrame = _sdf->GetElement("robotBaseFrame")->Get<std::string>();
  frontLeftWheelLink = _sdf->GetElement("frontLeftWheelLink")->Get<std::string>();
  frontRightWheelLink = _sdf->GetElement("frontRightWheelLink")->Get<std::string>();
  rearLeftWheelLink = _sdf->GetElement("rearLeftWheelLink")->Get<std::string>();
  rearRightWheelLink = _sdf->GetElement("rearRightWheelLink")->Get<std::string>();

  ROS_INFO("[sticking_force]: The sticking force magnitude is: %d,the advanced Mode: %d, the distributedForce: %d", magnitude, advancedMode, distributedForce);
  ROS_INFO("[sticking_force]: robotBaseFrame: %s", robotBaseFrame.c_str());
  ROS_INFO("[sticking_force]: front part links: %s %s", frontLeftWheelLink.c_str(), frontRightWheelLink.c_str());
  ROS_INFO("[sticking_force]: rear part links: %s %s", rearLeftWheelLink.c_str(), rearRightWheelLink.c_str());

  // Store the pointer to the model
  this->model = _parent;

  std::string modelName = model->GetName();
  //ROS_INFO("The name of the model is: %s", modelName.c_str());

  node = transport::NodePtr(new transport::Node());

  // Initiaize the node
  node->Init();

  if (distributedForce)
  {
    magnetic_field_front_left_wheel_Sub = node->Subscribe(local_prefix + "front_left_magnetic_field",
      &StickingForcePlugin::callback_magnetic_front_left_wheel, this);
    magnetic_field_front_right_wheel_Sub = node->Subscribe(local_prefix + "front_right_magnetic_field",
      &StickingForcePlugin::callback_magnetic_front_right_wheel, this);
    magnetic_field_rear_left_wheel_Sub = node->Subscribe(local_prefix + "rear_left_magnetic_field",
      &StickingForcePlugin::callback_magnetic_rear_left_wheel, this);
    magnetic_field_rear_right_wheel_Sub = node->Subscribe(local_prefix + "rear_right_magnetic_field",
      &StickingForcePlugin::callback_magnetic_rear_right_wheel, this);

    normal_front_left_wheel_Sub = node->Subscribe(local_prefix + "contact-surface/front_left_normal",
      &StickingForcePlugin::callback_normal_front_left_wheel, this);
    normal_front_right_wheel_Sub = node->Subscribe(local_prefix + "contact-surface/front_right_normal",
      &StickingForcePlugin::callback_normal_front_right_wheel, this);
    normal_rear_left_wheel_Sub = node->Subscribe(local_prefix + "contact-surface/rear_left_normal",
      &StickingForcePlugin::callback_normal_rear_left_wheel, this);
    normal_rear_right_wheel_Sub = node->Subscribe(local_prefix + "contact-surface/rear_right_normal",
      &StickingForcePlugin::callback_normal_rear_right_wheel, this);
  } else {
    net_normal_Sub = node->Subscribe(local_prefix + "contact-surface/net-normal",
      &StickingForcePlugin::callback_net_force, this);
    magnetic_field_Sub = node->Subscribe(local_prefix + "magnetic_field",
      &StickingForcePlugin::callback_magnetic, this);
  }


  // Listen to the update event. This event is broadcast every
  // simulation iteration.
  this->updateConnection = event::Events::ConnectWorldUpdateBegin(
    boost::bind(&StickingForcePlugin::OnUpdate, this, _1));
  //ROS_INFO("[sticking_force]: Sticking Force Plugin: Loaded succesfully");
}

// --------------------- OnUpdate ---------------------

// Called by the world update start event
void StickingForcePlugin::OnUpdate(const common::UpdateInfo & /*_info*/)
{
  gazebo::physics::Link_V linkList = model->GetLinks();
  int baseFrameIndex;
  int frontLeftWheelIndex, frontRightWheelIndex;
  int rearLeftWheelIndex, rearRightWheelIndex;


  // Debuging logging
  /*ROS_INFO("[sticking_force]: The number of links are: %ld", linkList.size());

  for (unsigned i = 0; i < linkList.size(); i++) {
    ROS_INFO("[sticking_force]: [%d]: %s", i, (linkList[i]->GetName()).c_str());
  }*/

  // identify the wheel links
  if (distributedForce)
  {
    for (unsigned i = 0; i < linkList.size(); i++) {
      if (linkList[i]->GetName() == frontLeftWheelLink) {
        //ROS_INFO("[sticking_force]: fl: %d", i);
        frontLeftWheelIndex = i;
      } else if (linkList[i]->GetName() == frontRightWheelLink) {
        //ROS_INFO("[sticking_force]: fr: %d", i);
        frontRightWheelIndex = i;
      } else if (linkList[i]->GetName() == rearLeftWheelLink) {
        //ROS_INFO("[sticking_force]: rl: %d", i);
        rearLeftWheelIndex = i;
      } else if (linkList[i]->GetName() == rearRightWheelLink) {
        //ROS_INFO("[sticking_force]: rr: %d", i);
        rearRightWheelIndex = i;
      }
    }

    // assign the links
    physics::LinkPtr frontLeftWheelLinkPtr = linkList[frontLeftWheelIndex];
    physics::LinkPtr frontRightWheelLinkPtr = linkList[frontRightWheelIndex];
    physics::LinkPtr rearLeftWheelLinkPtr = linkList[rearLeftWheelIndex];
    physics::LinkPtr rearRightWheelLinkPtr = linkList[rearRightWheelIndex];

    // apply sticking force id magnetic fielf exists of sticking everythere is enabled (no advancedMode)
    if(magneticField_FrontLeftWheel_Enabled == 1 || advancedMode == 0) {
      frontLeftWheelLinkPtr->SetForce(stickyForceOnFrontLeftWheel);
      //ROS_INFO("[sticking_force]: Apply the sticky force to the Front Left of the robot");
    }

    // apply sticking force id magnetic fielf exists of sticking everythere is enabled (no advancedMode)
    if(magneticField_FrontRightWheel_Enabled == 1 || advancedMode == 0) {
      frontRightWheelLinkPtr->SetForce(stickyForceOnFrontRightWheel);
      //ROS_INFO("[sticking_force]: Apply the sticky force to the Front Right of the robot");
    }

    // apply sticking force id magnetic fielf exists of sticking everythere is enabled (no advancedMode)
    if(magneticField_RearLeftWheel_Enabled == 1 || advancedMode == 0) {
      rearLeftWheelLinkPtr->SetForce(stickyForceOnRearLeftWheel);
      //ROS_INFO("[sticking_force]: Apply the sticky force to the Rear Left of the robot");
    }

    // apply sticking force id magnetic fielf exists of sticking everythere is enabled (no advancedMode)
    if(magneticField_RearRightWheel_Enabled == 1 || advancedMode == 0) {
      rearLeftWheelLinkPtr->SetForce(stickyForceOnRearRightWheel);
      //ROS_INFO("[sticking_force]: Apply the sticky force to the Rear Right, %d", advancedMode);
    }
  } else {

    // identify the base link
    for (unsigned i = 0; i < linkList.size(); i++) {
      if (linkList[i]->GetName() == robotBaseFrame) {
        //ROS_INFO("%d", i);
        baseFrameIndex = i;
      }
    }

    physics::LinkPtr baseFrameLinkPtr = linkList[baseFrameIndex];

    // apply sticking force id magnetic fielf exists of sticking everythere is enabled (no advancedMode)
    if(netMagneticFieldEnabled == 1 || advancedMode == 0)
    {
      //ROS_INFO("[sticking_force]: Apply the sticky force to the Base Frame of the robot");
      baseFrameLinkPtr->SetForce(stickyForce);
    }
  }


}

GZ_REGISTER_MODEL_PLUGIN(StickingForcePlugin)
