#include "net_normalForce_calc.hh"

using namespace gazebo;

NetForceCalcPlugin::NetForceCalcPlugin() : WorldPlugin()
{
  front_left_wheel = ignition::math::Vector3d(0, 0, 0);
  front_right_wheel = ignition::math::Vector3d(0, 0, 0);
  rear_left_wheel = ignition::math::Vector3d(0, 0, 0);
  rear_right_wheel = ignition::math::Vector3d(0, 0, 0);
}

NetForceCalcPlugin::~NetForceCalcPlugin()
{
}

void NetForceCalcPlugin::callback1(ConstVector3dPtr &_msg)
{
  front_left_wheel = ignition::math::Vector3d(_msg->x(), _msg->y(), _msg->z());
  //ROS_INFO("Normal front_left: [%f, %f, %f]", _msg->x(), _msg->y(), _msg->z());
}

void NetForceCalcPlugin::callback2(ConstVector3dPtr &_msg)
{
  front_right_wheel = ignition::math::Vector3d(_msg->x(), _msg->y(), _msg->z());
}

void NetForceCalcPlugin::callback3(ConstVector3dPtr &_msg)
{
  rear_left_wheel = ignition::math::Vector3d(_msg->x(), _msg->y(), _msg->z());
}

void NetForceCalcPlugin::callback4(ConstVector3dPtr &_msg)
{
  rear_right_wheel = ignition::math::Vector3d(_msg->x(), _msg->y(), _msg->z());
}


void NetForceCalcPlugin::Load(physics::WorldPtr _parent, sdf::ElementPtr /*_sdf*/)
{
  std::string local_prefix = "/gazebo/default/contact-surface/";
  node = transport::NodePtr(new transport::Node());

  // Initiaize the node
  node->Init();

  // Listen to Gazebo world_stats topic
  normalSub1 = node->Subscribe(local_prefix + "front_left_normal", &NetForceCalcPlugin::callback1, this);
  normalSub2 = node->Subscribe(local_prefix + "front_right_normal", &NetForceCalcPlugin::callback2, this);
  normalSub3 = node->Subscribe(local_prefix + "rear_left_normal", &NetForceCalcPlugin::callback3, this);
  normalSub4 = node->Subscribe(local_prefix + "rear_right_normal", &NetForceCalcPlugin::callback4, this);

  netNormalPub = node->Advertise<msgs::Vector3d>("~/contact-surface/net-normal");

  // Listen to the update event. This event is broadcast every
  // simulation iteration.
  this->updateConnection = event::Events::ConnectWorldUpdateBegin(
  boost::bind(&NetForceCalcPlugin::OnUpdate, this, _1));
}

// Called by the world update start event
void NetForceCalcPlugin::OnUpdate(const common::UpdateInfo & /*_info*/)
{
  //ROS_INFO("Normal front_left: [%f, %f, %f]", front_left_wheel.X(), front_left_wheel.Y(), front_left_wheel.Z());
  //ROS_INFO("Normal front_right: [%f, %f, %f]", front_right_wheel.X(), front_right_wheel.Y(), front_right_wheel.Z());
  //ROS_INFO("Normal rear_left: [%f, %f, %f]", rear_left_wheel.X(), rear_left_wheel.Y(), rear_left_wheel.Z());
  //ROS_INFO("Normal rear_right: [%f, %f, %f]", rear_right_wheel.X(), rear_right_wheel.Y(), rear_right_wheel.Z());
  ignition::math::Vector3d net_normal =  ignition::math::Vector3d();
  net_normal = front_right_wheel + front_left_wheel + rear_right_wheel + rear_left_wheel;
  net_normal = net_normal.Normalize();
  //ROS_INFO("[net_normalForce]: Net_normal force on vehicle: [%f, %f, %f]", net_normal.X(), net_normal.Y(), net_normal.Z());

  msgs::Vector3d pv;
  pv.set_x(net_normal.X());
  pv.set_y(net_normal.Y());
  pv.set_z(net_normal.Z());

  netNormalPub->Publish(pv);
}

// Register this plugin with the simulator
GZ_REGISTER_WORLD_PLUGIN(NetForceCalcPlugin)
