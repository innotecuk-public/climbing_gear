#include "contact_sensor.hh"

using namespace gazebo;

ContactPlugin::ContactPlugin() : SensorPlugin()
{
}

ContactPlugin::~ContactPlugin()
{
}

void ContactPlugin::Load(sensors::SensorPtr _sensor, sdf::ElementPtr _sdf)
{
  topic_prefix = _sdf->GetElement("prefix")->Get<std::string>();
  active_wheel = atoi((_sdf->GetElement("activeWheel")->Get<std::string>()).c_str());

  ROS_INFO("[contact_sensor]: ContactPlugin has prefix:%s and active_wheel flag:%d", topic_prefix.c_str(), active_wheel);

  std::string topic_normal_name = "~/contact-surface/" + topic_prefix + "_normal";
  std::string topic_magnetic_name = "~/" + topic_prefix + "_magnetic_field";

  node = transport::NodePtr(new transport::Node());

  // Initiaize the node
  node->Init();

  normalPub = node->Advertise<msgs::Vector3d>(topic_normal_name);
  magneticFieldPub = node->Advertise<msgs::Int>("~/magnetic_field");
  magneticFieldPerWheelPub = node->Advertise<msgs::Int>(topic_magnetic_name);

  // Get the parent sensor
  this->parentSensor = std::dynamic_pointer_cast<sensors::ContactSensor>(_sensor);

  // Make sure the parent sensor is valid.
  if(!this->parentSensor)
  {
    ROS_ERROR("ContactPlugin requires a ContactSensor.");
    return;
  }

  // Connect to the sensor update event.
  this->updateConnection = this->parentSensor->ConnectUpdated(
    std::bind(&ContactPlugin::OnUpdate, this));

  // Make sure the parent sensor is active.
  this->parentSensor->SetActive(true);
  //this->parentSensor->SetUpdateRate(1000.0);

  //ROS_INFO("[contact_sensor]: Contact Plugin: Loaded succesfully");
}

void ContactPlugin::OnUpdate()
{
  //ROS_INFO("[contact_sensor]: OnUpdate started");
  msgs::Vector3d normalVector;
  msgs::Contacts contacts;
  int contactSize;

  msgs::Int on, off;
  on.set_data(1);
  off.set_data(0);

  contacts = this->parentSensor->Contacts();
  contactSize = contacts.contact_size();

  int magnetic[contactSize];

  for (unsigned int i = 0; i < contactSize; i++)
  {
    std::string object1 = contacts.contact(i).collision1();
    std::string object2 = contacts.contact(i).collision2();
    std::string mag = "mag";

    //ROS_INFO("[contact_sensor]: object1: %s, object2: %s", object1.c_str(), object2.c_str());

    if ((object1.find(mag) != std::string::npos) || (object2.find(mag) != std::string::npos))
    {
      magnetic[i] = 1;
      //ROS_INFO("[contact_sensor]: Contact Plugin: Magnetic field identified");
    } else {
      magnetic[i] = 0;
      //ROS_INFO("[contact_sensor]: Contact Plugin: No Magnetic field identified");
    }

    for (unsigned int j = 0; j < contacts.contact(i).position_size(); j++)
    {
      // update the normal vector direction
      normalVector.set_x(contacts.contact(i).normal(j).x());
      normalVector.set_y(contacts.contact(i).normal(j).y());
      normalVector.set_z(contacts.contact(i).normal(j).z());
      normalPub->Publish(normalVector);
      //std::cout << "Contact Plugin: Normal (" << normalVector.x() << ", " <<
      //normalVector.y() << ", "<< normalVector.z() << ")" << std::endl;
      //ROS_INFO("[contact_sensor]: Contact Plugin: Normal (%f, %f, %f)", normalVector.x(), normalVector.y(), normalVector.z());
    }
  }

  // check if any of the points which belong to the contact are enabled
  bool exists = false;
  for(int i = 0; i < contactSize; i++)
  {
    if (magnetic[i] == 1)
    {
      exists = true;
    }
  }

  if (exists) {
    // magnetic field exists
    if (active_wheel) {
      // we have magnetic wheel
      magneticFieldPerWheelPub->Publish(on);
      //ROS_INFO("[contact_sensor]: magnetic wheel ON");
    } else {
      // we have belly magnet
      magneticFieldPub->Publish(on);
      //ROS_INFO("[contact_sensor]: belly magnet ON");
    }
  } else {
    // No magnetic field exists
    if (contactSize != 0)
    {
      if (active_wheel) {
        // we have magnetic wheel
        magneticFieldPerWheelPub->Publish(off);
        //ROS_INFO("[contact_sensor]: magnetic wheel OFF");
      } else {
        // we have belly magnet
        magneticFieldPub->Publish(off);
        //ROS_INFO("[contact_sensor]: belly magnet OFF");
      }
    } else {
      //ROS_INFO("[contact_sensor]: no contact info available");
    }
  }

  //ROS_INFO("[contact_sensor]: OnUpdate ended");
}

GZ_REGISTER_SENSOR_PLUGIN(ContactPlugin)
