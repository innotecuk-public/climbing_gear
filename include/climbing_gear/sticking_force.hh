#ifndef _GAZEBO_STICKINGFORCE_PLUGIN_HH_
#define _GAZEBO_STICKINGFORCE_HH_

#include <string>
#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/common/common.hh>
#include "gazebo/transport/transport.hh"
#include "ignition/math/Vector3.hh"
#include <gazebo/msgs/msgs.hh>
#include <ros/ros.h>

namespace gazebo
{
  class StickingForcePlugin : public ModelPlugin
  {
    // Constructor
    public: StickingForcePlugin();

    // Destructor
    public: virtual ~StickingForcePlugin();

    //  Load the model plugin
    public: virtual void Load(physics::ModelPtr _parent, sdf::ElementPtr /*_sdf*/);

    // Called by the world update start event
    public: void OnUpdate(const common::UpdateInfo & /*_info*/);

    // ------------- Callbacks -------------
    private: void callback_net_force(ConstVector3dPtr&);
    private: void callback_magnetic(ConstIntPtr&);

    // get the magnetic field from every wheel (if wheel is active and distributedForce is ON)
    private: void callback_magnetic_front_left_wheel(ConstIntPtr&);
    private: void callback_magnetic_front_right_wheel(ConstIntPtr&);
    private: void callback_magnetic_rear_left_wheel(ConstIntPtr&);
    private: void callback_magnetic_rear_right_wheel(ConstIntPtr&);

    // get the normal force from every wheel (if wheel is active and distributedForce is ON)
    private: void callback_normal_front_left_wheel(ConstVector3dPtr&);
    private: void callback_normal_front_right_wheel(ConstVector3dPtr&);
    private: void callback_normal_rear_left_wheel(ConstVector3dPtr&);
    private: void callback_normal_rear_right_wheel(ConstVector3dPtr&);

    // Node
    private: transport::NodePtr node;

    // ------------- Subscribers -------------
    private: transport::SubscriberPtr net_normal_Sub; // net normal
    private: transport::SubscriberPtr magnetic_field_Sub; // common magnetic topic

    // listen to magnetic fields on the wheels (if wheel is active and distributedForce is ON)
    private: transport::SubscriberPtr magnetic_field_front_left_wheel_Sub;
    private: transport::SubscriberPtr magnetic_field_front_right_wheel_Sub;
    private: transport::SubscriberPtr magnetic_field_rear_left_wheel_Sub;
    private: transport::SubscriberPtr magnetic_field_rear_right_wheel_Sub;

    // listen to normal forces on the wheels (if wheel is active and distributedForce is ON)
    private: transport::SubscriberPtr normal_front_left_wheel_Sub;
    private: transport::SubscriberPtr normal_front_right_wheel_Sub;
    private: transport::SubscriberPtr normal_rear_left_wheel_Sub;
    private: transport::SubscriberPtr normal_rear_right_wheel_Sub;

    // Pointer to the model
    private: physics::ModelPtr model;

    // Chassis
    private: physics::LinkPtr chassis_link;

    // Pointer to the update event connection
    private: event::ConnectionPtr updateConnection;

    // Forces
    private: int magnitude;
    private: ignition::math::Vector3d stickyForce;
    private: ignition::math::Vector3d netNormalForce;

    private: ignition::math::Vector3d stickyForceOnFrontLeftWheel;
    private: ignition::math::Vector3d stickyForceOnFrontRightWheel;
    private: ignition::math::Vector3d stickyForceOnRearLeftWheel;
    private: ignition::math::Vector3d stickyForceOnRearRightWheel;

    // normal force direction on wheels
    private: ignition::math::Vector3d front_left_wheel_normal;
    private: ignition::math::Vector3d front_right_wheel_normal;
    private: ignition::math::Vector3d rear_left_wheel_normal;
    private: ignition::math::Vector3d rear_right_wheel_normal;

    // Magnetic field
    private: int advancedMode; // when every surface has a magnetic property defined
    private: int netMagneticFieldEnabled;
    private: int magneticField_FrontLeftWheel_Enabled;
    private: int magneticField_FrontRightWheel_Enabled;
    private: int magneticField_RearLeftWheel_Enabled;
    private: int magneticField_RearRightWheel_Enabled;
    private: int distributedForce;

    // Robot links
    private: std::string robotBaseFrame;
    private: std::string frontLeftWheelLink;
    private: std::string frontRightWheelLink;
    private: std::string rearLeftWheelLink;
    private: std::string rearRightWheelLink;
  };
}
#endif
