#ifndef _GAZEBO_CONTACT_PLUGIN_HH_
#define _GAZEBO_CONTACT_PLUGIN_HH_

#include <string>
#include <gazebo/gazebo.hh>
#include <gazebo/sensors/sensors.hh>
#include "gazebo/transport/transport.hh"

#include <ros/ros.h>

namespace gazebo
{
  class ContactPlugin : public SensorPlugin
  {
    // Constructor
    public: ContactPlugin();

    // Destructor
    public: virtual ~ContactPlugin();

    //  Load the sensor plugin
    public: virtual void Load(sensors::SensorPtr _sensor, sdf::ElementPtr _sdf);

    // Callback that receives the contact sensor's update signal.
    private: virtual void OnUpdate();

    // Pointer to the contact sensor
    private: sensors::ContactSensorPtr parentSensor;

    // Connection that maintains a link between the contact sensor's
    // updated signal and the OnUpdate callback.
    private: event::ConnectionPtr updateConnection;

    // Add the node and the publisher
    private: transport::NodePtr node;
    private: transport::PublisherPtr normalPub; // publish the normal
    private: transport::PublisherPtr magneticFieldPub; // publish into common magnetic field topic
    private: transport::PublisherPtr magneticFieldPerWheelPub; // publish into wheel's magnetic field

    // Variables loaded from the URDF gazebo element of the plugin
    private: std::string topic_prefix;
    private: int active_wheel;
  };
}
#endif
