#ifndef _GAZEBO_NETNORMALFORCECALC_PLUGIN_HH_
#define _GAZEBO_NETNORMALFORCECALC_PLUGIN_HH_

#include <string>
#include <gazebo/gazebo.hh>
#include "gazebo/transport/transport.hh"
#include "ignition/math/Vector3.hh"

#include <ros/ros.h>

namespace gazebo
{
  class NetForceCalcPlugin : public WorldPlugin
  {
    // Constructor
    public: NetForceCalcPlugin();

    // Destructor
    public: virtual ~NetForceCalcPlugin();

    //  Load the sensor plugin.
    public: virtual void Load(physics::WorldPtr _parent, sdf::ElementPtr /*_sdf*/);

    // Callbacks
    private: void callback1(ConstVector3dPtr&);
    private: void callback2(ConstVector3dPtr&);
    private: void callback3(ConstVector3dPtr&);
    private: void callback4(ConstVector3dPtr&);

    // Node
    private: transport::NodePtr node;

    // Subscribers
    private: transport::SubscriberPtr normalSub1;
    private: transport::SubscriberPtr normalSub2;
    private: transport::SubscriberPtr normalSub3;
    private: transport::SubscriberPtr normalSub4;

    // Publisher
    private: transport::PublisherPtr netNormalPub;

    public: void OnUpdate(const common::UpdateInfo & /*_info*/);

    // normal force direction on wheels
    private: ignition::math::Vector3d front_left_wheel;
    private: ignition::math::Vector3d front_right_wheel;
    private: ignition::math::Vector3d rear_left_wheel;
    private: ignition::math::Vector3d rear_right_wheel;

    // Pointer to the update event connection
    private: event::ConnectionPtr updateConnection;
  };
}
#endif
