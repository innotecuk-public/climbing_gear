# climbing_gear

This is the source code for the climbing-gear package

## Instuctions

Please read the manual in the docs folder


## Setup Configuration

### Software
Ubuntu Xenial 16.04,

ROS Kinetic,

Gazebo 7.8


## Contributing

### author and maintainer

Angelos Plastropoulos (angelos.plastropoulos@innotecuk.com)